import React, { Component } from "react";

export default class GioHang extends Component {
  renderTbody = () => {
    return this.props.gioHang.map((item) => {
      if (item.soLuong !== 0) {
        return (
          <tr>
            <td>{item.name}</td>
            <td>{item.price}</td>
            <td>
              {" "}
              <img src={item.image} alt="" style={{ width: 80 }} />
            </td>
            <td>
              <button
                className="btn btn-primary"
                onClick={() => this.props.tru(item.id)}
              >
                -
              </button>
              <span>{item.soLuong}</span>
              <button
                className="btn btn-success"
                onClick={() => this.props.cong(item.id)}
              >
                +
              </button>
            </td>
            <td>
              <button
                className="btn btn-danger"
                onClick={() => this.props.xoa(item.id)}
              >
                X
              </button>
            </td>
          </tr>
        );
      }
    });
  };
  render() {
    return (
      <div className="container py-5">
        <table className="table">
          <thead>
            <tr>
              <td>Tên</td>
              <td>Giá</td>
              <td>Hình ảnh</td>
              <td>Số Lượng</td>
              <td>Xoá</td>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
        {this.props.gioHang.length == 0 && (
          <p className=" mt-5 text-center">Chưa có sản phẩm trong giỏi hàng</p>
        )}
      </div>
    );
  }
}
