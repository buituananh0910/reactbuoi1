import React, { Component } from 'react'

export default class Demo_State extends Component {
    state = {
        isLogin : true,
        username: "bta",
        soLuong:1,
    };
    HandleLogout = () =>{
        this.setState({
            isLogin: false,
        });
    };
    HandleLogin = () =>{
        this.setState({
            isLogin: true,
        }); 
    };
    HandleCong = () =>{
        this.setState({
            soLuong: this.state.soLuong+1,
        })
    }
    HandleTru = () => {
        this.setState({
            soLuong: this.state.soLuong-1,
        });
    };
  render() {
    return (
    <div>
        {this.state.isLogin ? ( <button onClick={this.HandleLogout} className='btn btn-success'>logout</button> ) : ( <button onClick={this.HandleLogin} className='btn btn-warning'>login</button> )}
        <div>
            <button onClick={this.HandleTru} className='btn btn-secondary'> - </button>
            <span>{this.state.soLuong}</span>
            <button onClick={this.HandleCong} className='btn btn-warning'> + </button>
        </div>
    </div>
    
    )
  }
}
