import React, { Component } from "react";
import "./css/LayoutComponent.css";
export default class BaiTapLayoutComponent extends Component {
  render() {
    return (
      <div>
        <header style={{ backgroundColor: "black" }}>
          <div
            className="container px-lg-5"
            style={{
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <div className="logo">
              <a
                href=""
                style={{
                  textDecoration: "none",
                  color: "white",
                  lineHeight: "50px",
                }}
              >
                Star Bootstrap
              </a>
            </div>
            <div className="navbar">
              <a
                href=""
                className="mr-3"
                style={{
                  textDecoration: "none",
                  color: "white",
                }}
              >
                Home
              </a>
              <a
                href=""
                className="mr-3"
                style={{
                  textDecoration: "none",
                  color: "white",
                  opacity: "0.5",
                }}
              >
                About
              </a>
              <a
                href=""
                className="mr-3"
                style={{
                  textDecoration: "none",
                  color: "white",
                  opacity: "0.5",
                }}
              >
                Contact
              </a>
            </div>
          </div>
        </header>
        <body className="mt-5">
          <div
            className="banner container d-flex px-lg-5"
            style={{
              flexDirection: "column",
              textAlign: "center",
              justifyContent: "center",
              backgroundColor: "whitesmoke",
              height: "300px",
            }}
          >
            <h2 style={{ fontSize: "50px" }}>A warm welcome!</h2>
            <p>
              Bootstrap utility classes are used to create this jumbotron since
              the old component has been removed from the framework. Why create
              custom CSS when you can use utilities?
            </p>
            <a className="btn btn-primary btn-lg" style={{ width: "20%" }}>
              Call to action
            </a>
          </div>
          <div className="item container mt-5">
            <div className="row">
              <div className="card col-3" style={{ width: "18rem" }}>
                <img
                  className="card-img-top"
                  src=".../100px180/"
                  alt="Card image cap"
                />
                <div className="card-body">
                  <h5 className="card-title">Card title</h5>
                  <p className="card-text">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </p>
                  <a href="#" className="btn btn-primary">
                    Go somewhere
                  </a>
                </div>
              </div>
              <div className="card col-3" style={{ width: "18rem" }}>
                <img
                  className="card-img-top"
                  src=".../100px180/"
                  alt="Card image cap"
                />
                <div className="card-body">
                  <h5 className="card-title">Card title</h5>
                  <p className="card-text">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </p>
                  <a href="#" className="btn btn-primary">
                    Go somewhere
                  </a>
                </div>
              </div>
              <div className="card col-3" style={{ width: "18rem" }}>
                <img
                  className="card-img-top"
                  src=".../100px180/"
                  alt="Card image cap"
                />
                <div className="card-body">
                  <h5 className="card-title">Card title</h5>
                  <p className="card-text">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </p>
                  <a href="#" className="btn btn-primary">
                    Go somewhere
                  </a>
                </div>
              </div>
              <div className="card col-3" style={{ width: "18rem" }}>
                <img
                  className="card-img-top"
                  src=".../100px180/"
                  alt="Card image cap"
                />
                <div className="card-body">
                  <h5 className="card-title">Card title</h5>
                  <p className="card-text">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </p>
                  <a href="#" className="btn btn-primary">
                    Go somewhere
                  </a>
                </div>
              </div>
            </div>
          </div>
        </body>
        <footer
          style={{
            backgroundColor: "black",
            height: "150px",
            marginTop: "50px",
          }}
        >
          <p
            style={{
              color: "white",
              lineHeight: "100px",
            }}
          >
            Copyright © Your Website 2022
          </p>
        </footer>
      </div>
    );
  }
}
