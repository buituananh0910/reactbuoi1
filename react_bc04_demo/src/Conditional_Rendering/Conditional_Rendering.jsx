import React, { Component } from 'react'

export default class Conditional_Rendering extends Component {
    isLogin = true;
    renderContent = () =>{
        if(this.isLogin){
            return <div>
                <p>hello user</p>
                <button onClick={this.HandleLogout} className='btn btn-warning'>Logout</button>
            </div>
        } else{
            return <div>
                <p>please login</p>
                <button>Login</button>
            </div>
        }
    }
    HandleLogout = () =>{
        this.isLogin=false;
        console.log(this.isLogin)
    }
  render() {
    return (
      <div>
        <p>Conditional_Rendering</p>
        {this.renderContent()}
      </div>
    )
  }
}
