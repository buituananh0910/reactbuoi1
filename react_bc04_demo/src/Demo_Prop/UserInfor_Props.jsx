import React, { Component } from "react";

export default class UserInfor_Props extends Component {
  changeName = () => {
    this.props.user = "bob";
  };
  render() {
    console.log(this.props);
    return (
      <div>
        UserInfor_Props
        <h2>Username : {this.props.user}</h2>
        <h2>Userage : {this.props.age}</h2>
        <button
          onClick={() => {
            this.props.changeName("tom");
          }}
          className="btn btn-danger"
        >
          change name
        </button>
      </div>
    );
  }
}
