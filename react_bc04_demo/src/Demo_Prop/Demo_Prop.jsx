import React, { Component } from "react";
import UserInfor_Props from "./UserInfor_Props";

export default class Demo_Prop extends Component {
  state = {
    username: "btaaaaaaaa",
    age: 2,
  };
  changeName = (name) => {
    this.setState({ username: name });
  };
  render() {
    return (
      <div>
        Demo_Prop
        <UserInfor_Props
          user={this.state.username}
          age={this.state.age}
          changeName={this.changeName}
        />
      </div>
    );
  }
}
